package account.web;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import account.web.Transaction.TransactionSummary;

import com.google.gson.JsonSyntaxException;

public class Account {
	
	private String accountNumber;
	private BigDecimal balance;
	
	private double totalDepositAmount = 0;
	private int totalDeposits = 0;
	private final int maxDepositsPerDay;
	private final double maxAmountPerDeposit;
	private final double maxDepositAmountPerDay;
	
	private double totalWithdrawnAmount = 0;
	private int totalWithdrawals = 0;
	private final int maxWithdrawalsPerDay;
	private final double maxAmountPerWithdrawal;
	private final double maxWithdawAmountPerDay;
	
	final String PATH = System.getProperty("catalina.base");
	final String balanceFile;
	
	public Account(String accountNumber) throws IllegalArgumentException {
		if(accountNumber == null || "".equals(accountNumber.trim())) {
			throw new IllegalArgumentException("Account number can't be missing");
		}
		
		this.accountNumber = accountNumber.trim();
		
		// load config
		this.maxDepositsPerDay = 4;
		this.maxAmountPerDeposit = 40000;
		this.maxDepositAmountPerDay = 150000;
		
		this.maxWithdrawalsPerDay = 3;
		this.maxAmountPerWithdrawal = 20000;
		this.maxWithdawAmountPerDay = 50000;
		
		this.balanceFile = PATH + accountNumber +"_bal";
		loadAccount(accountNumber);
		
		TransactionSummary summary = Transaction.loadTransactions();
		
		if(summary != null) {
			this.totalDepositAmount = summary.depositAmount;
			this.totalDeposits = summary.deposits;
			
			this.totalWithdrawnAmount = summary.withdrawAmount;
			this.totalWithdrawals = summary.withdrawals;
		}
	}
	
	public Account(String accountNumber, BigDecimal balance) {
		this(accountNumber);
		setBalance(balance);
	}
	
	public BigDecimal getBalance() {
		if(balance == null) {
			balance = BigDecimal.ZERO;
		}
		
		return balance;
	}
	
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void create() {
		// write account details to file
	}
	
	// create account if not exists
	public void loadAccount(String account) {
		File f = new File(balanceFile);
		System.out.println(f.getAbsolutePath());
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return;
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			
			String bal = br.readLine();
			balance = new BigDecimal(bal == null ? "0" : bal.trim());
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean deposit(BigDecimal deposit) throws DepositException, IllegalArgumentException, IOException {
		if(deposit.compareTo(BigDecimal.ZERO) == 1) {
			if(totalDeposits >= maxDepositsPerDay) {
				throw new DepositException("Exceeded maximum number of deposits per day");
			}
			
			if(totalDepositAmount < maxDepositAmountPerDay) {
				if(deposit.compareTo(new BigDecimal(maxDepositAmountPerDay - totalDepositAmount)) == 1) {
					throw new DepositException("Exceeded maximum deposit amount per day");
				}
			} else if(totalDepositAmount > maxDepositAmountPerDay) {
				throw new DepositException("Exceeded maximum deposit amount per day");
			}
			
			if(deposit.compareTo(new BigDecimal(maxAmountPerDeposit)) == 1) {
				throw new DepositException("Exceeded maximum deposit amount per transaction");
			}
			
			if(new Transaction(Transaction.DEPOSIT, deposit.doubleValue()).save()) {
				setBalance(getBalance().add(deposit));
				totalDeposits++;
				totalDepositAmount += deposit.doubleValue();
				
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(balanceFile)));
				bw.write(getBalance().toString());
				bw.flush();
				bw.close();
				
				return true;
			}
		} else {
			throw new IllegalArgumentException("Deposit amount should be more than 0");
		}
		
		return false;
	}
	
	public BigDecimal withdraw(BigDecimal amount) throws InsufficientFundsException, WithdrawalException, IOException {
		if(amount.compareTo(BigDecimal.ZERO) == 1) {
			if(totalWithdrawals >= maxWithdrawalsPerDay) {
				throw new WithdrawalException("Exceed maximum number of withdrawal transactions per day");
			}
		}
		
		if(getBalance().compareTo(amount) == -1) {
			throw new InsufficientFundsException("Insufficient funds");
		}
		
		if(totalWithdrawnAmount < maxWithdawAmountPerDay) {
			if(amount.compareTo(new BigDecimal(maxWithdawAmountPerDay - totalWithdrawnAmount)) == 1) {
				throw new WithdrawalException("Exceeded maximum withdrawal amount per day");
			}
		} else if(totalWithdrawnAmount > maxWithdawAmountPerDay) {
			throw new WithdrawalException("Exceeded maximum withdrawal amount per day");
		}
		
		if(amount.compareTo(new BigDecimal(maxAmountPerWithdrawal)) == 1) {
			throw new WithdrawalException("Exceeded maximum withdrawal amount per transaction");
		}
		
		if(new Transaction(Transaction.WITHDRAW, amount.doubleValue()).save()) {
			setBalance(getBalance().subtract(amount));
			totalWithdrawals++;
			totalWithdrawnAmount += amount.doubleValue();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(balanceFile)));
			bw.write(getBalance().toString());
			bw.flush();
			bw.close();
			
			return amount;
		}
		
		return BigDecimal.ZERO; 
	}
	
}
