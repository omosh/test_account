package account.web;

public class DepositException extends Exception {
	
	private static final long serialVersionUID = 3336935149011457729L;

	public DepositException() {
		super();
	}
	
	public DepositException(String message) {
		super(message);
	}
	
}
