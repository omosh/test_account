package account.web;

public class InsufficientFundsException extends Exception {

	private static final long serialVersionUID = -3450145365810609888L;
	
	public InsufficientFundsException() {
		super();
	}
	
	public InsufficientFundsException(String message) {
		super(message);
	}

}
