package account.web;

import java.math.BigDecimal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/account")
public class Service {
	
	@RequestMapping(value="balance/{acc}", method=RequestMethod.GET)
	public @ResponseBody Response balance(@PathVariable String acc) {
		Account account = new Account(acc);
		Response res = new Response();
		res.setAccount(acc);
		
		try {
			res.setCode(200);
			res.setMessage("Balance: " + account.getBalance().toString());
		} catch(Exception e) {
			e.printStackTrace();
			res.setCode(500);
			res.setMessage("Error occured. Please try again later");
		}
		
		return res;
	}
	
	@RequestMapping(value="/deposit/{amount}/{acc}", method=RequestMethod.GET)
	public @ResponseBody Response deposit(@PathVariable("acc") String acc, 
			@PathVariable("amount") double amount) {
		Account account = new Account(acc);
		Response res = new Response();
		
		res.setAccount(acc);
		
		try {
			res.setCode(200);
			if(account.deposit(new BigDecimal(amount))) {
				res.setMessage("deposit successful. New balance : " + account.getBalance().toString());
			} else {
				res.setMessage("deposit failed");
			}
		} catch (DepositException e) {
			e.printStackTrace();
			res.setMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode(500);
			res.setMessage("Error occured. Please try again later");
		}
		
		return res;
	}
	
	@RequestMapping(value="withdraw/{amount}/{acc}", method=RequestMethod.GET)
	public @ResponseBody Response withdraw(@PathVariable("acc") String acc, 
			@PathVariable("amount") double amount) {
		Account account = new Account(acc);
		Response res = new Response();
		
		res.setAccount(acc);
		
		try {
			res.setCode(200);
			if(account.withdraw(new BigDecimal(amount))
					.compareTo(new BigDecimal(amount)) == 0) {
				res.setMessage("withdraw success. New balance : " + account.getBalance().toString());
			} else {
				res.setMessage("withdraw failed.");
			}
		} catch (InsufficientFundsException | WithdrawalException e) {
			e.printStackTrace();
			res.setCode(400);
			res.setMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode(500);
			res.setMessage("Error occured. Please try again later");
		}
		
		return res;
	}
	
	public class Response {
		private int code;
		private String message;
		private String account;
		
		public String getAccount() {
			return account;
		}
		public void setAccount(String account) {
			this.account = account;
		}
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}
	
}
