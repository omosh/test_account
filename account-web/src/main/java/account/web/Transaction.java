package account.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Transaction {
	
	public static final int DEPOSIT = 1;
	public static final int WITHDRAW = 2;
	
	private int txnType = 0;
	private double amount;
	private Date dateTime;
	
	final static String PATH = System.getProperty("catalina.base");
	final static String txnfile = PATH + "_transactions";
	
	public Transaction(int txnType, double amount) throws IllegalArgumentException {
		if(txnType != DEPOSIT && txnType != WITHDRAW) {
			throw new IllegalArgumentException("txn type has to be either "
					+ "1. for deposit or 2. for withdrawal");
		}
		
		this.txnType = txnType;
		this.amount = amount;
		dateTime = new Date();
	}
	
	public Transaction(int txnType, double amount, Date dateTime) 
			throws IllegalArgumentException {
		if(dateTime == null) {
			throw new IllegalArgumentException("deposit datetime cannot be null");
		}
		
		this.amount = amount;
		this.dateTime = dateTime;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public Date getDateTime() {
		return dateTime;
	}
	
	public boolean isToday() {
		long td = dateTime.getTime() - new Date().getTime();
		return td > 0 && td <= (24 * 60 * 1000);
	}
	
	public boolean save() {
		TransactionSummary summary = loadTransactions();
		
		if(summary != null) {
			if(txnType == DEPOSIT) {
				summary.deposits++;
				summary.depositAmount += amount;
			} else if(txnType == WITHDRAW) {
				summary.withdrawals++;
				summary.withdrawAmount += amount;
			}
		} else {
			summary = new TransactionSummary();
			
			if(txnType == DEPOSIT) {
				summary.deposits = 1;
				summary.depositAmount = amount;
			} else if(txnType == WITHDRAW) {
				summary.withdrawals = 1;
				summary.withdrawAmount = amount;
			}
		}
		
		return writeTransactionFile(summary);
	}
	
	public boolean writeTransactionFile(TransactionSummary summary) {
		try {
			FileWriter fw = new FileWriter(txnfile);
			fw.write(new Gson().toJson(summary));
			fw.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	// loads current day's transactions only
	public static TransactionSummary loadTransactions() {
		TransactionSummary txns = null;
		
		try {
			File file = new File(txnfile);
			System.out.println(file.getAbsolutePath());
			if(file.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(file));
				txns = new Gson().fromJson(br.readLine(), TransactionSummary.class);
				br.close();
				return txns;
			}
		} catch (FileNotFoundException | JsonSyntaxException e) {
			e.printStackTrace();
			txns = new TransactionSummary();
			
			txns.deposits = 0;
			txns.depositAmount = 0;
			txns.withdrawals = 0;
			txns.withdrawAmount = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return txns;
	}
	
	public static class TransactionSummary {
		int withdrawals;
		double withdrawAmount;
		
		int deposits;
		double depositAmount;
		
		public TransactionSummary() {
			
		}
	}
	
}
