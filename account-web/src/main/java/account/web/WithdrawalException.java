package account.web;

public class WithdrawalException extends Exception {


	private static final long serialVersionUID = -2272463012521294297L;
	
	public WithdrawalException() {
		super();
	}
	
	public WithdrawalException(String message) {
		super(message);
	}

}
