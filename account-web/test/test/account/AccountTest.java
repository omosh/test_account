package test.account;

import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Test;

import account.web.Account;
import account.web.DepositException;
import account.web.InsufficientFundsException;
import account.web.WithdrawalException;


public class AccountTest {
	
	Account account = new Account("1234");
	
	@Test
	public void balance() {
		assertTrue(account.getBalance().doubleValue() >= 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void negativeDeposit() throws IllegalArgumentException, IOException {
		try {
			account.deposit(new BigDecimal(-1));
		} catch (DepositException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = DepositException.class)
	public void maxDepositAmountPerTransaction() throws IllegalArgumentException, DepositException, IOException {
		account.deposit(new BigDecimal(400001));
	}
	
	@Test(expected = DepositException.class)
	public void maxDailyDeposit() throws IllegalArgumentException, DepositException, IOException {
		account.deposit(new BigDecimal(1500001));
	}
	
	@Test(expected = DepositException.class)
	public void depositCount() throws IllegalArgumentException, DepositException, IOException {
		for(int i = 0; i < 10; i++) {
			account.deposit(new BigDecimal(1000));
			System.out.println("deposit " + (i + 1));
		}
	}
	
	@Test(expected = DepositException.class)
	public void trickyDeposit() throws IllegalArgumentException, DepositException, IOException {
		for(int i = 0; i < 10; i++) {
			if(i == 3) {
				account.deposit(new BigDecimal(1000 + 10000000));
			} else {
				account.deposit(new BigDecimal(1000));
			}
			System.out.println("deposit " + (i + 1));
		}
	}
	
	@Test
	public void normalDeposit() throws IOException {
		double tmp = account.getBalance().doubleValue();
		
		try {
			account.deposit(new BigDecimal(1000));
			assertTrue(account.getBalance().doubleValue() == (tmp + 1000));
		} catch (IllegalArgumentException | DepositException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test(expected = InsufficientFundsException.class)
	public void insufficentFunds() throws InsufficientFundsException, WithdrawalException, IOException {
		account.withdraw(account.getBalance().add(BigDecimal.ONE));
	}
	
	@Test(expected = WithdrawalException.class)
	public void overDraw() throws InsufficientFundsException, WithdrawalException, IOException {
		account.withdraw(new BigDecimal(40000));
	}
	
	@Test(expected = WithdrawalException.class)
	public void withdraw() throws WithdrawalException, InsufficientFundsException, IOException {
		account.withdraw(new BigDecimal(500001));
	}
	
	@Test(expected = WithdrawalException.class)
	public void trickyWithdraw() throws InsufficientFundsException, WithdrawalException, IOException {
		try {
			account.deposit(new BigDecimal(40000));
		} catch (IllegalArgumentException | DepositException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < 10; i++) {
			if(i == 3) {
				account.withdraw(account.getBalance().add(new BigDecimal(100)));
			} else {
				account.withdraw(new BigDecimal(100));
			}
			System.out.println("withdraw " + (i + 1));
		}
	}
	
}
